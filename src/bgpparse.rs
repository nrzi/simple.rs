use crate::bgp::{require, BGPError, Result};
use crate::bgpdata::{BgpMessage, BgpNotification, BgpOpen, BgpUpdate};
use crate::{read16, read32};

pub fn parse(t: u8, m: &[u8]) -> Result<BgpMessage> {
    match t {
        1 => parse_open(m),
        2 => parse_update(m),
        3 => parse_notification(m),
        4 => {
            require(m.len() == 0, "bad length in keepalive")?;
            Ok(BgpMessage::MsgKeepalive)
        }
        _ => Err(BGPError::ParseError(String::from("unknown message type"))),
    }
}

fn parse_open(m: &[u8]) -> Result<BgpMessage> {
    require(m.len() >= 10, "short length for open message")?;
    require(4 == m[0], "invalid BGP version")?;
    let opt_parm_len = m[9] as usize;
    require(m.len() == 10 + opt_parm_len, "invalid length for open message")?;

    Ok(BgpMessage::MsgOpen(BgpOpen {
        version: m[0],
        my_autonomous_system: read16(&m[1..3]),
        hold_time: read16(&m[3..5]),
        bgp_identifier: read32(&m[5..9]),
        optional_parameters: {
            let mut v = Vec::new();
            v.extend_from_slice(&m[9..]);
            v
        },
    }))
}

fn parse_notification(m: &[u8]) -> Result<BgpMessage> {
    require(m.len() >= 2, "short length for notification message")?;
    Ok(BgpMessage::MsgNotification(BgpNotification {
        error_code: m[0],
        error_subcode: m[1],
        data: {
            let mut v = Vec::new();
            v.extend_from_slice(&m[2..]);
            v
        },
    }))
}

fn parse_update(m: &[u8]) -> Result<BgpMessage> {
    // // first read the envelope - find and parse the attribute and NLRI sizes

    let payload_length = m.len();
    require(payload_length > 3, "update length error(1)")?;

    let withdrawn_routes_length = read16(&m[0..2]) as usize;
    require(payload_length >= 4 + withdrawn_routes_length, "update length error (2)")?;

    let total_path_attribute_length = read16(&m[2 + withdrawn_routes_length..4 + withdrawn_routes_length]) as usize;
    require(payload_length >= 4 + withdrawn_routes_length + total_path_attribute_length, "update length error (3)")?;

    // // ok - we got the partitioning OK - now parse the pieces

    Ok(BgpMessage::MsgUpdate(BgpUpdate {
        withdrawn: {
            let mut v = Vec::new();
            v.extend_from_slice(&m[2..2 + withdrawn_routes_length]);
            v
        },
        attributes: {
            let mut v = Vec::new();
            v.extend_from_slice(&m[4 + withdrawn_routes_length..4 + withdrawn_routes_length + total_path_attribute_length]);
            v
        },
        nlri: {
            let mut v = Vec::new();
            v.extend_from_slice(&m[4 + withdrawn_routes_length + total_path_attribute_length..]);
            v
        },
    }))
}
