use crate::bgp::{require, BGPError, Result};
use crate::bgpdata::BgpMessage;
use std::io::prelude::Read;

pub const MARKER: [u8; 16] = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff];

// Alternate design for incremental buffer reads, into client provided memory.  This avoids the double copy overheaed...

// here is the design for that:
// the input is an opened and possibly already part consumed ReadBuf stream
// the consumer does two reads - the hedaer read, then the body read.
// Simples.
// The freshly allocated raw buffer is the output.
// A more sophisticated consumer could allocate some other structure for onward transmission
// pub type Result<T> = std::result::Result<T, ParseError>;

fn read_frame<R: Read>(mut r: R) -> Result<(u8, Vec<u8>)> {
    // first allocate a header buffer

    let mut buffer = [0u8; 19];
    // then, read the header bytes - we need to use read exact to be sure that the buffer fills...
    r.read_exact(&mut buffer[..])?;

    // now, read the payload size....
    let msg_len = read16(&buffer[16..18]) as usize;

    // we can do some sanity checks too
    require(4097 > msg_len, &format!("message too long error {}", msg_len))?;
    require(&MARKER == &buffer[0..16], "invalid marker - synchronisation error?")?;
    let msg_type = buffer[18];
    require((msg_type > 0) && (msg_type < 6), &format!("invalid msg type {}", msg_type))?;

    // now we can make vec just large enough for the payload

    let mut payload: Vec<u8> = vec![0; msg_len - 19];

    // and read the remaining bytes into the buffer
    // which should not fail for want of bytes

    r.read_exact(&mut payload[..])?;
    Ok((msg_type, payload))
}

fn read16(slice: &[u8]) -> u16 {
    u16::from_be_bytes(unsafe { *(slice as *const [u8] as *const [u8; 2]) })
}

pub fn read_msg<R: Read>(mut r: R) -> Result<BgpMessage> {
    let (msg_type, payload) = read_frame(&mut r)?;
    crate::bgp::validate(msg_type, &payload[..])?;
    // crate::show_bgp(msg_type, &payload[..]);
    let msg = crate::bgpparse::parse(msg_type, &payload[..])?;
    Ok(msg)
}

pub fn read_parse<R: Read>(mut r: R) -> u64 {
    let mut msg_count = 0u64;
    loop {
        match read_frame(&mut r) {
            Err(BGPError::IoError(e)) if e.kind() == std::io::ErrorKind::UnexpectedEof => {
                eprintln!("normal EOF");
                break msg_count;
            }
            Err(e) => {
                eprintln!("unwanted error? {:?}", e);
                break msg_count;
            }
            Ok((msg_type, payload)) => {
                match crate::bgp::validate(msg_type, &payload[..]) {
                    Ok(()) => crate::show_bgp(msg_type, &payload[..]),
                    Err(e) => eprintln!("validator failed with {} for msg {}", e, msg_count),
                }
                msg_count += 1;
            }
        }
    }
}
