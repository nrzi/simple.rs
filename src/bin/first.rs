use simple::slice_parse;
use std::env;
use std::fs;

static FNAME: &str = "/home/nic/src/erlrisreplay/priv/updates.20200501.1135";

fn spercent(num: &u64, den: &u64) -> String {
    let r: u64 = (num * 10000) / den;
    let r: f64 = r as f64;
    format!("{}", r / 100f64)
}

fn main() {
    eprintln!("first file reader");
    let args: Vec<String> = env::args().collect();
    let fname: &str = if args.len() > 1 { &args[1] } else { FNAME };
    eprintln!("using {}", fname);
    let file = fs::read(fname).unwrap();
    let byte_count = file.len();
    eprintln!("the file {} has {} bytes", fname, byte_count);
    let mut zero_count: u64 = 0;
    for &b in &file {
        if 0 == b {
            zero_count += 1;
        };
    }
    eprintln!("the file has {} zero bytes", zero_count);
    eprintln!("that is {}%", 100f64 * zero_count as f64 / byte_count as f64);
    eprintln!("or, {}%", spercent(&zero_count, &(byte_count as u64)));
    slice_parse(&file)
}
