use std::env;
use std::fs::File;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
    let instant = std::time::Instant::now();
    eprintln!("second file reader");
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        let fname: &str = &args[1];
        eprintln!("using {}", fname);
        let f = File::open(fname)?;
        let reader = BufReader::new(f);
        let msg_count = simple::libx::read_parse(reader);
        eprintln!("{} msgs read, {} seconds elapsed", msg_count, instant.elapsed().as_secs_f32())
    } else {
        eprintln!("whoops please give a file name")
    };
    Ok(())
}
