use std::{fmt, io};

// -----------------------------------------------------------------------------------------------

// Parser error types and helper functions

pub type Result<T> = std::result::Result<T, BGPError>;

#[derive(Debug)]
pub enum BGPError {
    IoError(io::Error),
    ParseError(String),
}

impl From<io::Error> for BGPError {
    fn from(error: io::Error) -> Self {
        BGPError::IoError(error)
    }
}

impl BGPError {
    pub fn kind(&self) -> std::io::ErrorKind {
        std::io::ErrorKind::Other
    }
}

impl fmt::Display for BGPError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BGPError::IoError(ref err) => err.fmt(f),
            BGPError::ParseError(ref err) => write!(f, "Parsing error {:?}", err),
        }
    }
}

// helper functions

pub fn parse_error(in_string: &str) -> Result<()> {
    Err(BGPError::ParseError(String::from(in_string)))
}

pub fn require(p: bool, s: &str) -> Result<()> {
    if p {
        Ok(())
    } else {
        parse_error(s)
    }
}

// -----------------------------------------------------------------------------------------------

pub fn validate(t: u8, m: &[u8]) -> Result<()> {
    match t {
        1 => validate_open(m),
        2 => validate_update(m),
        3 => validate_notification(m),
        4 => require(m.len() == 0, "bad length in keepalive"),
        _ => parse_error("unknown message type"),
    }
}

fn validate_open(_m: &[u8]) -> Result<()> {
    Ok(())
}

fn validate_update(m: &[u8]) -> Result<()> {
    // first read the envelope - find and validate the attribute and NLRI sizes

    // require(false,"total nonsense in validate_update")?;

    let payload_length = m.len();
    require(payload_length > 3, "update length error(1)")?;

    let withdrawn_routes_length = read16(&m[0..2]) as usize;
    require(payload_length >= 4 + withdrawn_routes_length, "update length error (2)")?;

    let total_path_attribute_length = read16(&m[2 + withdrawn_routes_length..4 + withdrawn_routes_length]) as usize;
    require(payload_length >= 4 + withdrawn_routes_length + total_path_attribute_length, "update length error (3)")?;

    // ok - we got the partitioning OK - now validate the pieces
    validate_nlri(&m[2..2 + withdrawn_routes_length])?;
    validate_nlri(&m[4 + withdrawn_routes_length + total_path_attribute_length..])?;
    validate_path_attributes(&m[4 + withdrawn_routes_length..4 + withdrawn_routes_length + total_path_attribute_length])?;
    Ok(())
}

fn validate_nlri(m: &[u8]) -> Result<()> {
    let mut cursor = 0;
    let nlri_length = m.len();
    if nlri_length == 0 {
        Ok(())
    } else {
        loop {
            let pfx_length = ((m[cursor] as usize) + 15) / 8;
            require(6 > pfx_length, "NLRI prefix over-length")?;
            if nlri_length == cursor + pfx_length {
                break Ok(());
            } else if nlri_length > cursor + pfx_length {
                cursor += pfx_length
            } else {
                break parse_error("NLRI length error");
            }
        }
    }
}

fn validate_path_attributes(m: &[u8]) -> Result<()> {
    let mut cursor = 0;
    let attributes_length = m.len();
    let mut origin_flag = false;
    let mut as_path_flag = false;
    let mut next_hop_flag = false;
    while cursor < attributes_length {
        // check T +short V readable
        require(attributes_length - cursor > 2, "badly formatted path attributes(1)")?;

        let attr_flags = m[cursor];
        let attr_type_code = m[cursor + 1];
        let extended_flag: bool = 0x10 == (0x10 & attr_flags);

        // check T + long V readable if present
        require(!extended_flag || attributes_length - cursor > 3, "badly formatted path attributes(2)")?;

        let attr_length: usize = if extended_flag { read16(&m[cursor + 2..cursor + 4]) as usize } else { m[cursor + 2] as usize };

        // check entire TLV readable
        require(attributes_length >= cursor + attr_length + if extended_flag { 4 } else { 3 }, "badly formatted path attributes(3)")?;

        // the envelope is OK - what about the content?

        // the attribute specific code has to be inline in order to aggregate state across the attributes
        // e.g. mandatory parameter presence, and potentially duplicate checks

        match attr_type_code {
            1 => {
                origin_flag = true;
                require(attr_length == 1, "Bad ORIGIN length")?
            }
            2 => {
                as_path_flag = true;
                let v = if extended_flag { &m[cursor + 4..cursor + 4 + attr_length] } else { &m[cursor + 3..cursor + 3 + attr_length] };
                validate_as_path(attr_length, v)?
            }
            3 => {
                next_hop_flag = true;
                require(attr_length == 4, "Bad NextHop length")?
            }
            4 => require(attr_length == 4, "Bad MED length")?,
            5 => require(attr_length == 4, "Bad LOCAL_PREF length")?,
            6 => require(attr_length == 0, "Bad ATOMIC_AGGREGATE length")?,
            7 => require(attr_length == 8, "Bad AGGREGATOR length")?,
            21 => (), // 	AS_PATHLIMIT (deprecated)
            32 => require(attr_length % 12 == 0, "Bad LARGE_COMMUNITY length")?,
            _ => eprintln!("unhandled attribute {} length {}", attr_type_code, attr_length),
        };

        // do this last as we use the old value in the checks above
        cursor += attr_length + if extended_flag { 4 } else { 3 }
    }
    require(origin_flag, "missing Origin attribute")?;
    require(as_path_flag, "missing AS Path attribute")?;
    require(next_hop_flag, "missing Next Hop attribute")?;
    Ok(())
}

fn validate_as_path(l: usize, m: &[u8]) -> Result<()> {
    // now, this is a toy validator, so we can reject AS2
    // Which means that structure verification is easier
    // either the first segment is the only segment, so the overall length is 2 + 4 * byte[1], or
    // there is some spare at the end, and we can call ourselves recursively to handle it.
    require(l >= 2, "invalid AS segment size")?;
    let segment_type = m[0];
    require(2 == segment_type || 1 == segment_type, "invalid AS segment type")?;
    let segment_length = 2 + 4 * m[1] as usize;
    require(l >= segment_length, "invalid AS segment size(2)")?;
    if l == segment_length {
        Ok(())
    } else {
        // eprintln!("multi-segment path!");
        validate_as_path(l - segment_length, &m[segment_length..])
    }
}

fn validate_notification(_m: &[u8]) -> Result<()> {
    Ok(())
}

fn read16(slice: &[u8]) -> u16 {
    u16::from_be_bytes(unsafe { *(slice as *const [u8] as *const [u8; 2]) })
}
