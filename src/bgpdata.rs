use std::fmt;

pub enum BgpMessage {
    MsgUpdate(BgpUpdate),
    MsgOpen(BgpOpen),
    MsgNotification(BgpNotification),
    MsgKeepalive,
}

pub struct BgpUpdate {
    pub withdrawn: Vec<u8>,
    pub nlri: Vec<u8>,
    pub attributes: Vec<u8>,
}
pub struct BgpOpen {
    // commented code for real multi option support
    pub version: u8, // probably should not make this variable!
    pub my_autonomous_system: u16,
    pub hold_time: u16,
    pub bgp_identifier: u32,
    pub optional_parameters: Vec<u8>,
    // optional_parameters: Vec<Vec<u8>>, // wrong really: 1) as we only encode capabilities should be called for that
    //                                                     2) capability should be an enum - this is a quick fudge
}
pub struct BgpNotification {
    pub error_code: u8,
    pub error_subcode: u8,
    pub data: Vec<u8>,
}

impl fmt::Display for BgpMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            BgpMessage::MsgOpen(m) => write!(f, "BGP Open Message (as:{} bgpid:{} holdtime:{}", m.my_autonomous_system, m.bgp_identifier, m.hold_time),
            BgpMessage::MsgUpdate(_bgp_update) => write!(f, "BGP Update Message"),
            BgpMessage::MsgNotification(m) => write!(f, "BGP Notification Message ({},{})", m.error_code, m.error_subcode),
            BgpMessage::MsgKeepalive => write!(f, "BGP Keepalive Message"),
        }
    }
}

// pub fn build_open(asn: u32, bgpid: u32) -> BgpOpen {
pub fn build_open(asn: u32, bgpid: u32) -> BgpMessage {
    // commented code for real multi option support
    // let mut v = Vec::new();
    // v.push(encode_capability_as4(asn).to_vec());
    BgpMessage::MsgOpen(BgpOpen {
        version: 4,
        my_autonomous_system: if asn > 65535 { 23456 } else { asn as u16 },
        hold_time: 0,
        bgp_identifier: bgpid,
        optional_parameters: encode_capability_as4(asn).to_vec(),
        // optional_parameters: v
    })
}

fn encode_capability_as4(asn: u32) -> Vec<u8> {
    let mut v = [0x02, 0x06, 0x41, 0x4].to_vec(); // length then wrapped tlv headers cap (02/06) as4 (41/4)
                                                  // let mut v = [0x41, 0x4].to_vec();
    v.extend_from_slice(&asn.to_be_bytes());
    v
}

fn encode_bgp_open(bgp_open: BgpOpen) -> Vec<u8> {
    // commented code for real multi option support
    let mut v = [bgp_open.version].to_vec();
    v.extend_from_slice(&bgp_open.my_autonomous_system.to_be_bytes());
    v.extend_from_slice(&bgp_open.hold_time.to_be_bytes());
    v.extend_from_slice(&bgp_open.bgp_identifier.to_be_bytes());
    assert!(bgp_open.optional_parameters.len() < 256);
    v.push(bgp_open.optional_parameters.len() as u8);
    v.extend(bgp_open.optional_parameters);
    // for p in bgp_open.optional_parameters {v.extend(p)} // this doesn't work becuase it needs to capture the total byte length of the capabilities and put that into both the options length (+2)
    // and in the capabilties length (TLV with T=2)
    v
}

fn encode_bgp_notification(bgp_notification: BgpNotification) -> Vec<u8> {
    let mut v = Vec::new();
    v.push(bgp_notification.error_code);
    v.push(bgp_notification.error_subcode);
    v.extend(bgp_notification.data);
    v
}

fn encode_bgp_update(bgp_update: BgpUpdate) -> Vec<u8> {
    let mut v = Vec::new();
    v.extend_from_slice(&(bgp_update.withdrawn.len() as u16).to_be_bytes());
    v.extend(bgp_update.withdrawn);
    v.extend_from_slice(&(bgp_update.attributes.len() as u16).to_be_bytes());
    v.extend(bgp_update.attributes);
    v.extend(bgp_update.nlri);
    v
}

pub fn encode_bgp_message(bgp_message: BgpMessage) -> Vec<u8> {
    let (t, l, v): (u8, u16, Vec<u8>) = match bgp_message {
        BgpMessage::MsgOpen(bgp_open) => {
            let v = encode_bgp_open(bgp_open);
            let l = v.len() as u16;
            (1, l, v)
        }
        BgpMessage::MsgUpdate(bgp_update) => {
            let v = encode_bgp_update(bgp_update);
            let l = v.len() as u16;
            (2, l, v)
        }
        BgpMessage::MsgNotification(bgp_notification) => {
            let v = encode_bgp_notification(bgp_notification);
            let l = v.len() as u16;
            (3, l, v)
        }
        BgpMessage::MsgKeepalive => (4u8, 0, Vec::new()),
    };
    let mut m = Vec::new();
    m.extend_from_slice(&crate::libx::MARKER);
    m.extend_from_slice(&(l + 19).to_be_bytes());
    m.push(t);
    m.extend(v);
    m
}
