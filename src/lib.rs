use std::io::prelude::Read;

pub mod bgp;
pub mod bgpdata;
pub mod bgpparse;
pub mod libx;

pub fn slice_parse(d: &[u8]) {
    let mut rest = d;
    while 18 < rest.len() {
        let (msg_type, msg, r) = parse_bgp(&rest).expect("failed to get a message");
        show_bgp(msg_type, msg);
        rest = r;
    }
}

pub fn raw_read_parse<R: Read>(mut d: R) -> u64 {
    let mut buffer = Vec::new();
    let mut msg_count = 0u64;
    d.read_to_end(&mut buffer).unwrap();
    let mut rest = &buffer[..];
    while 18 < rest.len() {
        let (msg_type, msg, r) = parse_bgp(&rest).expect("failed to get a message");
        show_bgp(msg_type, msg);
        msg_count += 1;
        rest = r;
    }
    msg_count
}

const LIMIT: usize = 4096 * 64;

// question - how to make this generic?
// the loop consumes bytes using slice operations-
// when there is insufficient data remaining the inner loop exits so that the
// buffer can be refilled
// When the underlying stream retruns empty then the outer loop exits
// a deconstructed version should separate the consumer and possibly the buffer refill too.

// step 1 - a parser has as input a byteslice and as output a Result whcih wraps a tuple of type (consumed_byte_count, parsed message)
// in the future the return type can be Partial / Ok / Error
// the Ok returns a single parsed item, and the length it parsed

// step II - the parser is an interator
// why is this hard?
// the iterator must retrurn a representation of the parsed item, which cannot
// contain a reference to the underlying buffer, because the compiler cannot control
// the context in which the iterator output is used..unwrap()
// So a miinimal parser which returns slices from the underlying buffer is insufficient
// Th eproblem is more fundamental though: - any pull based access has the same problem,
// unless it provides a mutable input structure to hold/copy the parsed output.
// Hence we have to make independent structures or copies.
// OR
// Use incremental buffer reads, into client provided memory.  This avoids the double copy overheaed...

// here is the design for that:
// the input is an opened and possibly already part consumed ReadBuf stream
// the consumer does two reads - the hedaer read, then the body read.
// Simples.
// The freshly allocated raw buffer is the output.
// A more sophisticated consumer could allocate some other structure for onward transmission

pub fn read_parse<R: Read>(mut r: R) -> u64 {
    // this incremental reader will read on a buffer:
    // if the read count is zero then the stream is empty and the functions should return/complete
    // in start frame mode if the read count is less than 19 a second read is required:
    let mut buffer = [0u8; LIMIT];
    let mut cursor: usize = 0;
    let mut grc: usize = 0;
    let mut msg_count = 0u64;
    loop {
        // after first loop there may be remaining bytes in the buffer, between [cursor] and [rc]
        // this is only not true if cursor == rc
        // after the copy we need to set the base for reading into to the number of bytes copied ( rc - cursor), and reset the cursor to zero.
        let have = grc - cursor;
        (&mut buffer[..]).copy_within(cursor..grc, 0);
        match r.read(&mut buffer[have..LIMIT]) {
            Err(e) => panic!("unwanted io error {}", e),
            Ok(0) => break msg_count,
            Ok(rc) => {
                grc = have + rc;
                cursor = 0;
                let slice = &buffer[..];
                loop {
                    // should only check for end of stream if you care about reporting partial messages
                    // otherwise the early exit is safe
                    if grc - cursor < 19 {
                        // eprintln!("want 19 have {}", grc - cursor);
                        break;
                    } else {
                        let msg_type = slice[cursor + 18];
                        assert!((msg_type > 0) && (msg_type < 6), "invalid msg type {}", msg_type);
                        let msg_len = read16(&slice[cursor + 16..cursor + 18]) as usize;
                        if grc - cursor < msg_len {
                            // eprintln!("want {} have {}", msg_len, grc - cursor);
                            break;
                        } else {
                            // show_bgp(msg_type, &slice[cursor+19..cursor+msg_len-19]);
                            msg_count += 1;
                            cursor += msg_len;
                        }
                    }
                }
            }
        }
    }
}

pub fn read16(slice: &[u8]) -> u16 {
    u16::from_be_bytes(unsafe { *(slice as *const [u8] as *const [u8; 2]) })
}

pub fn read32(slice: &[u8]) -> u32 {
    u32::from_be_bytes(unsafe { *(slice as *const [u8] as *const [u8; 4]) })
}

fn parse_bgp(d: &[u8]) -> Option<(u8, &[u8], &[u8])> {
    if d.len() == 0 {
        None
    } else if d.len() < 19 {
        eprintln!("parse error");
        None
    } else {
        let msg_type = d[18];
        assert!((msg_type > 0) && (msg_type < 6), "invalid msg type {}", msg_type);

        let msg_len = read16(&d[16..18]) as usize;
        assert!(msg_len <= d.len(), "message length error: {} > {}", msg_len, d.len());
        Some((msg_type, &d[19..msg_len], &d[msg_len..]))
    }
}

pub fn show_bgp(t: u8, m: &[u8]) {
    let msg_len = m.len();
    let display_len = std::cmp::min(60, msg_len);
    let decode_p = bgpparse::parse(t, &m);
    match decode_p {
        Ok(decode) => println!("{} len: {} body: {}", decode, msg_len, hex::encode(&m[..display_len])),
        Err(e) => println!("Parse failed: {} len: {} type: {} body: {}", e, msg_len, t, hex::encode(&m[..display_len])),
    }
}
